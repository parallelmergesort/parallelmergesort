﻿namespace ParallelMergeSort
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading.Tasks;

    class Program
    {
        /// <summary>
        /// Parallel Merge Sort
        /// </summary>
        public static class MergeSortAlgorithm
        {
            /// <summary>
            /// Represents the optimal size of the list where parallel sort (multi-threading) is more
            /// performant than sequential. This parameter may vary dependeng on the computer charaterist
            /// </summary>
            private static int parallelThreshold = 1000;
            /// <summary>
            /// Used to limit the number of tasks to be created while doing recursive parallel sorting.
            /// It's not useful to create many task than there are processors to run them.
            /// depthRemaining = log2(NCores) + 4 whene NCores is the number of machine cores.
            /// </summary>
            private static int depthRemaining = (int)Math.Log(Environment.ProcessorCount, 2) + 4;
            /// <summary>
            /// Sort a given list of integers in ascending order using parallel merge sort
            /// </summary>
            /// <param name="list">
            /// List to be sorted.
            /// </param>
            public static void ParallelMergeSort(ref List<int> list)
            {
                ParallelMergeSort(ref list, depthRemaining);
            }
            /// <summary>
            /// Recursive method for parallel merge sort
            /// </summary>
            /// <param name="list">List to be sorted</param>
            /// <param name="depthRemaining">Number of remaining recursive calls before switching
            /// to sequential sort instead of parallel
            /// </param>
            public static void ParallelMergeSort(ref List<int> list, int depthRemaining)
            {
                // If list size is 0 (empty) or 1, consider it sorted and return it
                if (list.Count <= 1)
                {
                    return;
                }
                // Else list size is > 1, so split the list into two sublists.
                int middleIndex = (list.Count) / 2;
                var left = list.GetRange(0, middleIndex);
                var right = list.GetRange(middleIndex, list.Count - middleIndex);
                if (list.Count > parallelThreshold && depthRemaining > 0)
                {
                    depthRemaining--;
                    // Recursively call ParallelMergeSort for splitted sublists
                    Parallel.Invoke(
                    () => ParallelMergeSort(ref left, depthRemaining),
                    () => ParallelMergeSort(ref right, depthRemaining)
                    );
                }
                else
                {
                    // Recursively call ParallelMergeSort in sequential manner until sublist size is 1.
                    ParallelMergeSort(ref left, 0);
                    ParallelMergeSort(ref right, 0);
                }
                // Merge the sublists sorted in prior calls to ParallelMergeSort()
                list = Merge(left, right);
            }
            /// <summary>
            /// Merge two lists into one sorted list
            /// </summary>
            /// <param name="leftList"></param>
            /// <param name="rightList"></param>
            /// <returns></returns>
            public static List<int> Merge(List<int> leftList, List<int> rightList)
            {
                List<int> resultList = new List<int>();
                // While the sublist are not empty merge them repeatedly to produce new sublists
                // until there is only 1 sublist remaining. This will be the sorted list.
                while (leftList.Count > 0 || rightList.Count > 0)
                {
                    if (leftList.Count > 0 && rightList.Count > 0)
                    {
                        // Compare the 2 lists, append the smaller element to the result list
                        // and remove it from the original list.
                        if (leftList[0] <= rightList[0])
                        {
                            resultList.Add(leftList[0]);
                            leftList.RemoveAt(0);
                        }
                        else
                        {
                            resultList.Add(rightList[0]);
                            rightList.RemoveAt(0);
                        }
                    }
                    else if (leftList.Count > 0)
                    {
                        resultList.Add(leftList[0]);
                        leftList.RemoveAt(0);
                    }
                    else if (rightList.Count > 0)
                    {
                        resultList.Add(rightList[0]);
                        rightList.RemoveAt(0);
                    }
                }
                return resultList;
            }
        }


        static void Main(string[] args)
        {
            int listSize = 500000;
            var myList = new List<int>();
            var rand = new Random();
            for (int i = 0; i < listSize; i++)
            {
                myList.Add(rand.Next(listSize - 1));
            }

            // Create new stopwatch.
            Stopwatch stopwatch = new Stopwatch();

            // Begin timing.
            stopwatch.Start();

            MergeSortAlgorithm.ParallelMergeSort(ref myList);

            // Stop timing.
            stopwatch.Stop();

            // Write result.
            Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);
            Console.ReadKey();
        }
    }
}
